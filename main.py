'''
program to compute past and future cooldowns in cooldown channel
cooldowns possible: 10 mins, 30 mins, 1 hour, 2 hours, 24 hours, 7 days, is permanently banned from CS:GO servers, Account is Untrusted, Account Convicted
three main variables shelved to store: timeRestoredToGamers, totalTemporaryLs, totalPermanentLs
'''

import asyncio
import discord
from discord.ext import commands
from discord_slash import SlashCommand
from discord_slash.utils.manage_commands import create_option, create_choice
import ocrspace
import shelve
import re

from settings import *
# ocrSpaceKey = ""

# discordBotToken = ""
# discordRoleID = ""
# channels = ["cooldowns"]
guild_ids = [824310661382275102]

db = shelve.open("main")

# initial setup, comment it out after 1st run and db is initialised
db["timeRestoredToGamers"] = 2168540
db["totalTemporaryLs"] = 2440
db["totalPermanentLs"] = 17

ocrSpaceAPI = ocrspace.API(ocrSpaceKey)

client = discord.Client()
client = commands.Bot(command_prefix=")")
slash = SlashCommand(client, sync_commands=True)

minsToHours = {10: "10 minute", 30: "30 minute", 60: "1 hour", 120: "2 hours", 1440: "24 hours", 10080: "7 days"}
patternsToMatch = ["10", "30", "1", "2", "24", "7", "untrusted", "convicted", "permanently banned"]

def parseResponse(message, response, op):
    messageResponse = ""
    for pattern in patternsToMatch:
        regxd = re.findall("(?i)"+pattern, response, flags=0)
        for _ in range(len(regxd)):
            success, result = parsePattern(pattern, op)
            messageResponse += responseTime(message, success, result, op)[0]
    if messageResponse == "":
        messageResponse += responseTime(message, False, "nout", "")
    return messageResponse

def funnyCalc(a, b, op):
    expression = str(a) + op + str(b)
    result = eval(expression)
    return result

def parsePattern(pattern, op):
    cooldownDict = {"10":10, "30":30, "1":60, "2":120, "24":1440, "7":10080}
    if pattern in cooldownDict:
        banTime = cooldownDict[pattern]
        db["timeRestoredToGamers"] = funnyCalc(db["timeRestoredToGamers"], banTime, op)
        db["totalTemporaryLs"] = funnyCalc(db["totalTemporaryLs"], 1, op)
        return True, minsToHours[banTime]

    elif pattern == "untrusted" or pattern == "convicted" or pattern == "permanently banned":
        db["totalPermanentLs"] += 1
        return True, "ban"
    
    else:
        return False, ""

def doTheVeryCoolYes(message, url):
    response = parseResponse(message, ocrSpaceAPI.ocr_url(url), "+")
    return response

def responseTime(message, success, result, op, manual=False):
    if success:
        return "Success! {0} was {1}.\nCurrent cooldown total at: {2} minutes, {3} cooldowns, and {4} bans.".format(result, op, db["timeRestoredToGamers"], db["totalTemporaryLs"], db["totalPermanentLs"]), True
    elif manual:
        return "Message {0.jump_url} failed to process, {0.author.mention} is a clown.\n".format(message), False
    else:
        return "Message {0.jump_url} failed to process, {0.author.mention} is a clown. {1} check this shit out\n".format(message, message.guild.get_role(discordRoleID).mention), False

def isGuildOwner():
    def predicate(ctx):
        return ctx.guild is not None and ctx.guild.owner_id == ctx.author.id
    return commands.check(predicate)

def is_me(m):
    return m.author == client.user

@client.command(help="Adds cooldown/ban. Input one of the following: 10, 30, 1, 2, 24, 7, untrusted, convicted, permanently banned",brief="Adds a cooldown/ban.")
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def add(ctx, arg):
    success, result = parsePattern(arg, "+")
    temp = responseTime(ctx.message, success, result, "+", manual=True)
    if temp[1]:
        await ctx.reply(temp[0], delete_after = 10)
        await ctx.message.delete()
    else:
        await ctx.reply(temp[0])

@client.command(help="Removes cooldown/ban. Input one of the following: 10, 30, 1, 2, 24, 7, untrusted, convicted, permanently banned",brief="Removes a cooldown/ban.")
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def remove(ctx, arg):
    success, result = parsePattern(arg, "-")
    temp = responseTime(ctx.message, success, result, "-", manual=True)
    if temp[1]:
        await ctx.reply(temp[0], delete_after = 10)
        await ctx.message.delete()
    else:
        await ctx.reply(temp[0])

@client.command(help="Adds manual amount of cooldown(s). Input: Number of minutes of cooldown to add and amount of cooldowns to add",brief="Adds a cooldown/ban.")
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def manualAdd(ctx, arg1, arg2=1):
    db["timeRestoredToGamers"] += int(arg1)
    db["totalTemporaryLs"] += int(arg2)
    temp = responseTime(ctx.message, True, arg1, "+", manual=True)
    if temp[1]:
        await ctx.reply(temp[0], delete_after = 10)
        await ctx.message.delete()
    else:
        await ctx.reply(temp[0])

@client.command(help="Removes manual amount of cooldown(s). Input: Number of minutes of cooldown to remove and amount of cooldowns to remove",brief="Removes a cooldown/ban.")
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def manualRemove(ctx, arg1, arg2=1):
    db["timeRestoredToGamers"] -= int(arg1)
    db["totalTemporaryLs"] -= int(arg2)
    temp = responseTime(ctx.message, True, arg1, "-", manual=True)
    if temp[1]:
        await ctx.reply(temp[0], delete_after = 10)
        await ctx.message.delete()
    else:
        await ctx.reply(temp[0])

@client.command(help="Shows the statistics of the current totalling of this channel",brief="Shows stats.")
async def stats(ctx):
    await ctx.send("Total minutes of cooldowns handed out: {0}\nTotal cooldowns handed out: {1}\nTotal bans handed out: {2}".format(db["timeRestoredToGamers"],db["totalTemporaryLs"], db["totalPermanentLs"]), delete_after = 30)
    await ctx.message.delete()

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if str(message.channel) in channels:
        if not "idiot" in [y.name.lower() for y in message.author.roles]:
            if hasattr(message, "attachments"):
                for a in message.attachments:
                    cool = doTheVeryCoolYes(message, a.proxy_url)
                    if cool[1]:
                        await message.reply(cool[0], delete_after = 10)
                    else:
                        await message.reply(cool[0])
            # if hasattr(message, "embeds"):

            # if hasattr(message, "embeds"):
            #     for e in message.embeds:
            #         print(e.image.proxy_url)
            #         print(type(e.image.proxy_url))
            #         print(type(e))
            #         print(e)
            #         # await message.channel.send("received url, it is".format(message.Embed.image.proxy_uurl))
            #         # await message.reply(doTheVeryCoolYes(message, message.Embed.image.proxy_url))
            print(message)
        else:
            await message.delete()
            await message.channel.send("haha clown", delete_after = 10)
            await asyncio.sleep(10)
    
    await client.process_commands(message)

@client.event
async def on_ready():
    print("cool, we {} and we good".format(client.user))

    game = discord.Game("with your mum | use )help")
    await client.change_presence(status=discord.Status.idle, activity=game)


@slash.slash(name="stats", guild_ids=guild_ids, description="check stats ye")
async def slashStats(ctx):
    await ctx.send("Total minutes of cooldowns handed out: {0}\nTotal cooldowns handed out: {1}\nTotal bans handed out: {2}".format(db["timeRestoredToGamers"],db["totalTemporaryLs"], db["totalPermanentLs"]), delete_after = 30)

@slash.slash(name="manualRemove", guild_ids=guild_ids, description="Removes manual amount of cooldown(s).",
            options=[
                create_option(
                    name="minutes",
                    description="Amount of minutes to remove.",
                    option_type=4,
                    required=True
               ),
                create_option(
                    name="cooldowns",
                    description="Amount of cooldowns to remove.",
                    option_type=4,
                    required=False
               )
             ])
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def slashManualRemove(ctx, minutes, cooldowns=1):
    db["timeRestoredToGamers"] -= int(minutes)
    db["totalTemporaryLs"] -= int(cooldowns)
    temp = responseTime(ctx.message, True, minutes, "-", manual=True)
    if temp[1]:
        await ctx.send(temp[0], delete_after = 10)

    else:
        await ctx.send(temp[0], delete_after = 30)

@slash.slash(name="manualAdd", guild_ids=guild_ids, description="Adds manual amount of cooldown(s).",
            options=[
                create_option(
                    name="minutes",
                    description="Amount of minutes to add.",
                    option_type=4,
                    required=True
               ),
                create_option(
                    name="cooldowns",
                    description="Amount of cooldowns to add.",
                    option_type=4,
                    required=False
               )
             ])
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def slashManualAdd(ctx, minutes, cooldowns=1):
    db["timeRestoredToGamers"] += int(minutes)
    db["totalTemporaryLs"] += int(cooldowns)
    temp = responseTime(ctx.message, True, minutes, "+", manual=True)
    if temp[1]:
        await ctx.send(temp[0], delete_after = 10)
    else:
        await ctx.send(temp[0], delete_after = 30)

@slash.slash(name="remove", guild_ids=guild_ids, description="Removes cooldown/ban.",
            options=[
                create_option(
                    name="_type",
                    description="10, 30, 1, 2, 24, 7, untrusted, convicted, permanently banned",
                    option_type=3,
                    required=True,
                    choices=[
                        create_choice(
                        name="10 minutes",
                        value="10"
                    ),
                        create_choice(
                        name="30 minutes",
                        value="30"
                    ),
                        create_choice(
                        name="1 hour",
                        value="1"
                    ),
                        create_choice(
                        name="2 hours",
                        value="2"
                    ),
                        create_choice(
                        name="24 hours",
                        value="24"
                    ),
                        create_choice(
                        name="7 days",
                        value="7"
                    ),
                        create_choice(
                        name="Ban (any perm)",
                        value="convicted"
                    )]
               ),
             ])
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def slashRemove(ctx, _type):
    success, result = parsePattern(_type, "-")
    temp = responseTime(ctx.message, success, result, "-", manual=True)
    if temp[1]:
        await ctx.send(temp[0], delete_after = 10)
    else:
        await ctx.send(temp[0])

@slash.slash(name="add", guild_ids=guild_ids, description="Adds cooldown/ban.",
            options=[
                create_option(
                    name="_type",
                    description="10, 30, 1, 2, 24, 7, untrusted, convicted, permanently banned",
                    option_type=3,
                    required=True,
                    choices=[
                        create_choice(
                        name="10 minutes",
                        value="10"
                    ),
                        create_choice(
                        name="30 minutes",
                        value="30"
                    ),
                        create_choice(
                        name="1 hour",
                        value="1"
                    ),
                        create_choice(
                        name="2 hours",
                        value="2"
                    ),
                        create_choice(
                        name="24 hours",
                        value="24"
                    ),
                        create_choice(
                        name="7 days",
                        value="7"
                    ),
                        create_choice(
                        name="Ban (any perm)",
                        value="convicted"
                    )]
                )
             ])
@commands.check_any(commands.has_role("cooldown keeper".lower()), commands.has_role("strong".lower()), commands.is_owner(), isGuildOwner())
async def slashAdd(ctx, _type):
    success, result = parsePattern(_type, "+")
    temp = responseTime(ctx.message, success, result, "+", manual=True)
    if temp[1]:
        await ctx.send(temp[0], delete_after = 10)
    else:
        await ctx.send(temp[0])


client.run(discordBotToken)