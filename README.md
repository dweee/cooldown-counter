# Cooldown counter

A discord bot to count cooldowns from screenshots through OCR (thanks ocr.space for an easy to use API and for the module creator to make it easy for a monkey like me!)

To be deployed in [DENZIL's disco](https://discord.gg/mXCmjCb6jP).

## Setup

figure it out yourself, shouldn't be hard from reading the top of `main.py` and `settings.py.example`

## To the users

```)stats to check stats.```

## To the cooldown keepers

```The main commands that you will need to use are )add and )remove in case the OCR doesn't pickup on the text properly, or in case there are false positives. )manualAdd and )manualRemove are mostly unneeded but are there to make it easy to add and remove custom amounts of time and cooldowns in case it is required.```