from signal import signal, SIGINT
from sys import exit

timeRestoredToGamers = 2168540
totalTemporaryLs = 2440
totalPermanentLs = 17

def parsePattern(pattern):
	global timeRestoredToGamers
	global totalPermanentLs
	global totalTemporaryLs

	if pattern == "10":
		timeRestoredToGamers -= 10
		totalTemporaryLs -= 1
	elif pattern == "30":
		timeRestoredToGamers -= 30
		totalTemporaryLs -= 1
	elif pattern == "1":
		timeRestoredToGamers -= 60
		totalTemporaryLs -= 1
	elif pattern == "2":
		timeRestoredToGamers -= 120
		totalTemporaryLs -= 1
	elif pattern == "24":
		timeRestoredToGamers -= 1440
		totalTemporaryLs -= 1
	elif pattern == "7":
		timeRestoredToGamers -= 10080
		totalTemporaryLs -= 1
	elif pattern == "untrusted" or pattern == "convicted" or pattern == "permanently banned" or pattern == "ban":
		totalPermanentLs -= 1

def handler(signal_received, frame):
	print('''\ntimeRestoredToGamers = {}
	totalTemporaryLs = {}
	totalPermanentLs = {}'''.format(timeRestoredToGamers, totalTemporaryLs, totalPermanentLs))
	print('SIGINT or CTRL-C detected. Exiting gracefully')
	exit(0)


if __name__ == '__main__':
	# Tell Python to run the handler() function when SIGINT is recieved
	signal(SIGINT, handler)

	print('Running. Press CTRL-C to exit.')
	while True:
		parsePattern(str(input("input: ")))
		print('''timeRestoredToGamers = {}\ntotalTemporaryLs = {}\ntotalPermanentLs = {}'''.format(timeRestoredToGamers, totalTemporaryLs, totalPermanentLs))
	